package com.example.popsicaltest.ui.main

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat.getSystemService
import androidx.lifecycle.*
import androidx.paging.DataSource
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import androidx.paging.PositionalDataSource
import com.example.popsicaltest.R
import com.example.popsicaltest.api.ApiInterface
import com.example.popsicaltest.api.RetrofitClient
import com.example.popsicaltest.api.model.Track
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import okhttp3.ResponseBody
import java.util.*


class MainVM : ViewModel() {
    private val apiService = RetrofitClient.getClient().create(ApiInterface::class.java)

    // for user inputs
    val pagedListTypeLD = MutableLiveData<PagedListTypeLD?>(null)

    // show paged list or single item (all/genre vs single)
    val singleTrackLD = MutableLiveData<Resource>()

    // config for paged list
    val pagedListConfig = PagedList.Config.Builder()
        .setEnablePlaceholders(false)
        .setInitialLoadSizeHint(30)
        .setPrefetchDistance(20)
        .setPageSize(10)
        .build()

    var pagedListLD: LiveData<PagedList<Track>>? = Transformations.switchMap(
        pagedListTypeLD
    ) {
        initializedPagedListBuilder(pagedListTypeLD.value).build()
    }

    private fun initializedPagedListBuilder(pagedListTypeLD: PagedListTypeLD?): LivePagedListBuilder<Int, Track> {
        val dataSourceFactory = object : DataSource.Factory<Int, Track>() {
            override fun create(): DataSource<Int, Track> {
                // differentiate between single track and paged list first
                return pagedListTypeLD?.singleTrack?.let {
                    SingleDataSource(listOf(it))

                } ?: kotlin.run { // single track is null, means list
                    MainDataSource(Dispatchers.IO, pagedListTypeLD)
                }
            }
        }

        return LivePagedListBuilder(dataSourceFactory, pagedListConfig)
    }

    fun onGenreSelected(view: View, position: Int) {
        val context = view.context

        pagedListTypeLD.value = if (position == 0)
            PagedListTypeLD(null, null, 1)
        else {
            val genreStr = context.resources.getStringArray(R.array.genre)[position]
            PagedListTypeLD(null, genreStr.toLowerCase(Locale.getDefault()), 1)
        }
    }

    fun getTrackByID(id: String?) {
        viewModelScope.launch {
            val response = apiService.getTrackById(id)

            singleTrackLD.value = Resource(
                response.isSuccessful,
                response.message(),
                response.errorBody(),
                response.body()
            )
        }
    }

    fun isInternetAvailable(context: Context): Boolean {
        var result = false
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            val networkCapabilities = connectivityManager.activeNetwork ?: return false
            val actNw =
                connectivityManager.getNetworkCapabilities(networkCapabilities) ?: return false
            result = when {
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
                actNw.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
                else -> false
            }
        } else {
            connectivityManager.run {
                connectivityManager.activeNetworkInfo?.run {
                    result = when (type) {
                        ConnectivityManager.TYPE_WIFI -> true
                        ConnectivityManager.TYPE_MOBILE -> true
                        ConnectivityManager.TYPE_ETHERNET -> true
                        else -> false
                    }

                }
            }
        }

        return result
    }
}

class SingleDataSource<T>(private val singleTrackList: List<T>) : PositionalDataSource<T>() {
    override fun loadInitial(params: LoadInitialParams, callback: LoadInitialCallback<T>) {
        callback.onResult(singleTrackList, 0, singleTrackList.size)
    }

    override fun loadRange(params: LoadRangeParams, callback: LoadRangeCallback<T>) {
        val start = params.startPosition
        callback.onResult(singleTrackList.subList(start, start))
    }
}

data class PagedListTypeLD(
    val singleTrack: Track?,
    val genre: String?,
    val page: Int?
)

data class Resource(
    val isSuccessful: Boolean,
    val msg: String,
    val error: ResponseBody?,
    val data: Any?
)