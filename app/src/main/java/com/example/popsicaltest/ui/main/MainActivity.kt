package com.example.popsicaltest.ui.main

import android.os.Bundle
import android.os.Handler
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SearchView
import androidx.appcompat.widget.SearchView.SearchAutoComplete
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.popsicaltest.R
import com.example.popsicaltest.api.model.Track
import com.example.popsicaltest.databinding.ActivityMainBinding


class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private var searchView: SearchView? = null
    private val mainVM: MainVM by viewModels()
    private val mainAdapter = MainAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.vm = mainVM
        setSupportActionBar(findViewById(R.id.toolbar))

        subscribePagedTracksObserver()
        subscribeToSingleTrackObserver()
        setupRecyclerView()
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.menu_main, menu)

        searchView = menu.findItem(R.id.action_search).actionView as SearchView?

        setupSearchQueryListener()

        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return when (item.itemId) {
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun setupRecyclerView() {
        binding.rvTracks.apply {
            val llm = LinearLayoutManager(this@MainActivity)
            layoutManager = llm
            adapter = mainAdapter
            addItemDecoration(DividerItemDecoration(this@MainActivity, llm.orientation))
        }
    }

    private val searchQueryHandler = Handler()
    private var oldSearchText = ""
    private fun setupSearchQueryListener() {
        Log.d("JAY_LOG", "MainActivity: setupSearchQueryListener ")

        searchView?.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                Log.d("JAY_LOG", "MainActivity: onQueryTextChange old = $oldSearchText, new = $newText")
                // don't do anything if just click (hvn't typed for this session)
                if (oldSearchText == newText) return false

                // remove search results if empty (reload paged list based on selected genre)
                if (newText.isNullOrEmpty()) {
                    mainVM.onGenreSelected(binding.root, binding.spinnerGenre.selectedItemPosition)
                    return false
                }

                searchQueryHandler.removeCallbacksAndMessages(null)
                searchQueryHandler.postDelayed({
                    oldSearchText = newText
                    Log.d("JAY_LOG", "MainActivity: onQueryTextChange newText = $newText")

                    if (mainVM.isInternetAvailable(this@MainActivity)) {
                        mainVM.getTrackByID(newText)
                    } else {
                        Toast.makeText(this@MainActivity, R.string.no_internet, Toast.LENGTH_SHORT)
                            .show()
                    }
                }, 300)

                return false
            }

        })

        searchView?.setOnCloseListener {
            // reload paged list on clear (only if was single track result)
            if (mainVM.singleTrackLD.value?.data != null) {
                mainVM.onGenreSelected(binding.root, binding.spinnerGenre.selectedItemPosition)
            }

            false
        }
    }

    private fun subscribePagedTracksObserver() {
        Log.d("JAY_LOG", "MainActivity: subscribePagedTracksObserver ")

        mainVM.pagedListLD?.observe(this, Observer {
            Log.d(
                "JAY_LOG",
                "MainActivity: subscribePagedTracksObserver first = ${it.firstOrNull()}"
            )

            mainAdapter.submitList(it)
        })
    }

    private fun subscribeToSingleTrackObserver() {
        mainVM.singleTrackLD.observe(this, Observer {
            if (it.isSuccessful) {
                // successful - show single track item
                mainVM.pagedListTypeLD.value = PagedListTypeLD(
                    singleTrack = it.data as Track?,
                    genre = null,
                    page = null
                )

            } else {
                // unsuccessful - not found
                Toast.makeText(this, R.string.track_not_found, Toast.LENGTH_LONG).show()
            }
        })
    }
}