package com.example.popsicaltest.ui.main

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.databinding.ViewDataBinding
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.popsicaltest.R
import com.example.popsicaltest.api.model.Track
import com.example.popsicaltest.databinding.TrackItemBinding
import kotlinx.android.synthetic.main.track_item.view.*

class MainAdapter : PagedListAdapter<Track, MainAdapter.MyViewHolder>(DiffUtilCallBack()) {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        return MyViewHolder(DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.track_item,
            parent, false) as TrackItemBinding)

//        return MyViewHolder(DataBindingUtil.inflate<ViewDataBinding>(LayoutInflater.from(parent.context), R.layout.track_item,
//            parent, false) as TrackItemBinding)
    }

    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it) }
    }

    class MyViewHolder(private val binding: TrackItemBinding) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Track) {
            with(binding) {
                vm = item
                executePendingBindings()
            }

            itemView.setOnClickListener{
                Toast.makeText(it.context, "go to track details page for id ${item.id}", Toast.LENGTH_SHORT).show()
            }
        }
    }
}

class DiffUtilCallBack : DiffUtil.ItemCallback<Track>() {
    override fun areItemsTheSame(oldItem: Track, newItem: Track): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: Track, newItem: Track): Boolean {
        return oldItem.title == newItem.title
    }

}