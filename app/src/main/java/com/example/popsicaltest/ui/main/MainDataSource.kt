package com.example.popsicaltest.ui.main

import android.util.Log
import androidx.paging.PageKeyedDataSource
import com.example.popsicaltest.api.ApiInterface
import com.example.popsicaltest.api.RetrofitClient
import com.example.popsicaltest.api.model.Track
import com.example.popsicaltest.api.model.Tracks
import kotlinx.coroutines.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.coroutines.CoroutineContext

class MainDataSource(
    coroutineContext: CoroutineContext,
    private val pagedListTypeLD: PagedListTypeLD?
) : PageKeyedDataSource<Int, Track>() {
    private val apiService = RetrofitClient.getClient().create(ApiInterface::class.java)

    private val job = Job()
    private val scope = CoroutineScope(coroutineContext + job)

    override fun loadInitial(
        params: LoadInitialParams<Int>,
        callback: LoadInitialCallback<Int, Track>
    ) {
        scope.launch {
            Log.d("JAY_LOG", "MainDataSource: loadInitial ")

            try {
                val response = getApiServiceByGenre(pagedListTypeLD?.genre, pagedListTypeLD?.page ?: 1)

                when {
                    response.isSuccessful -> {
                        val listing = response.body()

                        Log.d("JAY_LOG", "MainDataSource: loadInitial resp = $listing")

                        listing?.meta?.let { meta ->
                            val previous =
                                if (meta.current_page - 1 <= 0) 0 else meta.current_page - 1
                            val after =
                                if (meta.num_pages == meta.current_page) null else meta.current_page + 1

                            callback.onResult(listing.tracks, previous, after)
                        }
                    }

                    else -> {
                        Log.d(
                            "JAY_LOG",
                            "TracksDataSource: loadInitial unsuc = ${response.errorBody()}"
                        )
                    }
                }

            } catch (exception: Exception) {
                Log.d("JAY_LOG", "TracksDataSource: loadInitial exception = $exception")
            }

        }

    }

    override fun loadAfter(params: LoadParams<Int>, callback: LoadCallback<Int, Track>) {
        Log.d("JAY_LOG", "MainDataSource: loadAfter ")

        scope.launch {
            try {
                val response = getApiServiceByGenre(pagedListTypeLD?.genre, params.key)
                Log.d("JAY_LOG", "MainDataSource: loadAfter resp = $response")

                when {
                    response.isSuccessful -> {
                        val listing = response.body()

                        listing?.meta?.let { meta ->
                            val after =
                                if (meta.num_pages == meta.current_page) null else meta.current_page + 1

                            callback.onResult(listing.tracks, after)
                        }
                    }
                }

            } catch (exception: Exception) {
                Log.e("PostsDataSource", "Failed to fetch data!")
            }
        }
    }

    override fun loadBefore(
        params: LoadParams<Int>,
        callback: LoadCallback<Int, Track>
    ) {
        Log.d("JAY_LOG", "MainDataSource: loadBefore ")

        scope.launch {
            try {
                val response = getApiServiceByGenre(pagedListTypeLD?.genre, params.key)
                Log.d("JAY_LOG", "MainDataSource: loadBefore resp = $response")

                when {
                    response.isSuccessful -> {
                        val listing = response.body()

                        listing?.meta?.let { meta ->
                            val previous =
                                if (meta.current_page - 1 <= 0) 0 else meta.current_page - 1

                            callback.onResult(listing.tracks, previous)
                        }
                    }
                }

            } catch (exception: Exception) {
                Log.e("PostsDataSource", "Failed to fetch data!")
            }
        }
    }

    @ExperimentalCoroutinesApi
    override fun invalidate() {
        super.invalidate()
        scope.cancel()
    }

    private suspend fun getApiServiceByGenre(genre: String?, page: Int): Response<Tracks> {
        return if (genre == null)
            apiService.getTracksListAll(page = page)
        else
            apiService.getTracksListByGenre(genre = genre, page = page)
    }
}