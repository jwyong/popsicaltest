package com.example.popsicaltest.api

import com.example.popsicaltest.api.model.Artist

data class Track_artists (

	val type : String,
	val artist : Artist
)