package com.example.popsicaltest.api

import com.example.popsicaltest.api.model.Track
import com.example.popsicaltest.api.model.Tracks
import retrofit2.Call
import retrofit2.Response
import retrofit2.http.*

interface ApiInterface {
//    List of Tracks API:
//    https://api.popsical.tv/v3/songs.json
//
//    List of Tracks API filter by genre:
//    genres available: pop, rock, hip hop, country
//    https://api.popsical.tv/v3/songs.json?genres=pop
//
//    Show Track by ID:
//    https://api.popsical.tv/v3/songs/:id.json

    @GET("v3/songs.json")
    suspend fun getTracksListByGenre(
        @Query("genres") genre: String,
        @Query("page") page: Int
    ): Response<Tracks>

    @GET("v3/songs.json")
    suspend fun getTracksListAll(
        @Query("page") page: Int
    ): Response<Tracks>

    @GET("v3/songs/{id}.json")
    suspend fun getTrackById(
        @Path(value = "id", encoded = true) id: String?
    ): Response<Track?>
}