package com.example.popsicaltest.api

data class Block_reason (

	val country_blocked : Boolean,
	val platform_blocked : Boolean,
	val time_lapsed_blocked : Boolean
)