package com.example.popsicaltest.api.model

import com.example.popsicaltest.api.Block_reason
import com.example.popsicaltest.api.Images
import com.example.popsicaltest.api.Track_artists

data class Tracks(
    val tracks: MutableList<Track>,
    val meta: Meta
)

data class Track(
    val id: Int,
    val number: String,
    val title: String,
    val alt_title: String,
    val lang_code: String,
    val runtime: Int,
    val release_date: String,
    val has_video: Boolean,
    val genres: List<String>?,
    val source: String,
    val images: Images,
    val lyrics_count: Int,
    val block: Boolean,
    val original_lyric_url: String,
    val has_lrc: Boolean,
    val music_feature_music_feature_key: Int,
    val music_feature_is_major: Int,
    val block_reason: Block_reason,
    val track_artists: List<Track_artists>?
) {
    fun artistListToStr(): String {
        var concatStr = ""
        track_artists?.forEach {
            concatStr += if (concatStr == "")
                it.artist.name
            else
                ", ${it.artist.name}"
        }

        return concatStr
    }

    fun genreListToStr(): String {
        var concatStr = ""
        genres?.forEach {
            concatStr += if (concatStr == "")
                it
            else
                ", $it"
        }

        return concatStr
    }
}

data class Meta(
    val total: Int,
    val current_page: Int,
    val num_pages: Int,
    val per_page: Int,
    val previous: String,
    val next: String
)