package com.example.popsicaltest.api.model

import com.example.popsicaltest.api.Images

data class Artist (

	val id : Int,
	val name : String,
	val alt_name : String,
	val gender : String,
	val lang_codes : List<String>,
	val images : Images,
	val total_tracks : Int
)